/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class Municipio {

    private int id_municipio;
    private String nombre;
    private ListaCD<Persona> personas;

    public Municipio() {
    }

    public void insertarPersona(Persona nueva) {
        this.personas.insertarFin(nueva);
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
        this.personas = new ListaCD();
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    @Override
    public String toString() {
        String msg = "Nombre Municipio:" + this.getNombre();
        for (Persona p : this.personas) {
            msg += "\n" + p.toString();
        }
        return msg;
    }

}
